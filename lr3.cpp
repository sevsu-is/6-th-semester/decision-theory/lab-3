#include <iostream>

double mas_cost[2][4] = {
        // дискретные значения стоимости (критерий 1)
        {1.0/100,1.0/75,1.0/50,1.0/25},
        // полезность
        {0,1,2,3}
};

double mas_millage[2][4] = {
        // дискретные значения пробега (критерий 2)
        {1.0/80,1.0/60,1.0/40,1.0/20},
        // полезность
        {0,1,2,3}
};

int main() {
	float mass_U[10][3] = {
            // кривая безразличия 1
		{0,0,0},

            // кривая безразличия 2
		{1,0,1},
		{0,1,1},

            // кривая безразличия 3
		{1,1,2},
		{0,2,2},
		{2,0,2},

            // кривая безразличия 4
		{1,2,3},
		{0,3,3},
		{2,1,3},
		{3,0,3}
	};

	float jump=0;

	for (int i = 1; i < 10; i++) {
		for (int j = 0; j < 3; j++) {
            int prevTotalU = mass_U[i][2];
            int currTotalU = mass_U[i - 1][2];

            if (currTotalU == prevTotalU) {
                int diffBetweenK2 = mass_U[i][1] - mass_U[i-1][1];
                jump = diffBetweenK2 / (mass_U[i-1][0] - mass_U[i - 1][1] - mass_U[i][0] + mass_U[i][1]);
            }
		}
	}

	double cost[4] = { 1.0/45,1.0/30,1.0/20,1.0/55 };
	double millage[4] = { 1.0/30,1.0/50,1.0/80,1.0/25 };

	double a[3];
	double b[3];
	int i, j;

	std::cout << "Дискретные значения одномерных функций полезности и соответствующие значения критериев" << std::endl;
	std::cout << "k1: ";
	for (i = 0; i < 4; i++)
        std::cout << mas_millage[0][i] << " ";
	std::cout << std::endl;
	std::cout << "U1(k1): ";
	for (i = 0; i < 4; i++)
        std::cout << mas_millage[1][i] << " ";
	std::cout << std::endl << std::endl;

	std::cout << "k2: ";
	for (i = 0; i < 4; i++)
        std::cout << mas_cost[0][i] << " ";
	std::cout << std::endl;
	std::cout << "U2(k2): ";
	for (i = 0; i < 4; i++)
        std::cout << mas_cost[1][i] << " ";
	std::cout << std::endl << std::endl;

	//Расчёт коэффициентов для функций U1, U2, U3 с помощью методов наименьших квадратов
	double xiyi = 0;
	double xi = 0;
	double yi = 0;
	double xi2 = 0;

	std::cout << "Коэффициенты в аналитических одномерных функциях полезности U1(k1), U2(k2), " << std::endl << "найденные методом наименьших квадратов: " << std::endl;
	//U1(k1)
	xiyi = 0;
	xi = 0;
	yi = 0;
	xi2 = 0;
	for (i = 0; i < 4; i++) {
		xiyi = xiyi + mas_millage[0][i] * mas_millage[1][i];
		xi = xi + mas_millage[0][i];
		yi = yi + mas_millage[1][i];
		xi2 = xi2 + mas_millage[0][i] * mas_millage[0][i];
	}
	a[1] = (4 * xiyi - xi * yi) / (4 * xi2 - xi * xi);
	b[1] = (yi - a[1] * xi) / 4;
	std::cout << "U1(k1)=" << a[1] << "+(" << b[1] << "*k1)" << std::endl;
	double U1[4];

	for (i = 0; i < 4; i++) {
		U1[i] = a[1]  + b[1] *  millage[i];
	}
	for (i = 0; i < 4; i++)
		std::cout << "U1(k1)=" << U1[i] << std::endl;

	//U2(k2)
	xiyi = 0;
	xi = 0;
	yi = 0;
	xi2 = 0;
	for (i = 0; i < 4; i++) {
		xiyi = xiyi + mas_cost[0][i] * mas_cost[1][i];
		xi = xi + mas_cost[0][i];
		yi = yi + mas_cost[1][i];
		xi2 = xi2 + mas_cost[0][i] * mas_cost[0][i];
	}
	a[2] = (4 * xiyi - xi * yi) / (4 * xi2 - xi * xi);
	b[2] = (yi - a[2] * xi) / 4;
	std::cout << std::endl << "U2(k2)=" << a[2] << "+(" << b[2] << "*k2)" << std::endl;
	double U2[4];
	for (i = 0; i < 4; i++) {
		U2[i] = a[2] + b[2] *  cost[i];
	}
	for (i = 0; i < 4; i++)
	    std::cout << "U2(k2)=" << U2[i] << std::endl;


	double U_summ[4] = {};
	std::cout << std::endl << "U_summ" << std::endl;
	for (i = 0; i < 4; i++) {
		U_summ[i] = jump* U2[i] + (1-jump) * U1[i];
		std::cout << i + 1 << " " << U_summ[i] << std::endl;
	}

	double max = U_summ[0];
	int index;
	for (i = 1; i < 4; i++) {
		if (U_summ[i] > max) {
			max = U_summ[i];
			 index = i;
		}
	}

	std::cout << std::endl << "Максимальная полезность: " << max <<" при цене: " << 1/cost[index] << " и пробеге: " << 1/millage[index];
}